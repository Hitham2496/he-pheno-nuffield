{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# New Physics Searches at the Large Hadron Collider - Data Analysis"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To present your findings from comparisons of Standard Model predictions to **simulated** data for Higgs boson production, the steps of the computations are implemented in this notebook.\n",
    "\n",
    "The data we examine are for an experimental analysis for the process $pp\\rightarrow Hjj$ where $j$ represents a jet of hadrons. We aim to produce a distribution examining the mass of the two combined jets:\n",
    "\n",
    "$$\\frac{\\mathrm{d}\\sigma}{\\mathrm{d}m_{jj}},$$\n",
    "\n",
    "which is measured in units of $\\mathrm{pb/GeV}$. This will plotted in bins of $m_{jj}$, measured in $\\mathrm{GeV}$.\n",
    "\n",
    "In this notebook you will plot the Standard Model background against the experimental data to examine whether the data confidently indicates the presence of new physics.\n",
    "\n",
    "For further information on the background and project brief please refer to the project [webpage](https://hitham2496.gitlab.io/he-pheno-nuffield/)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Structuring data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Information can be stored in many form on computing systems, **data** needs to be **structured** in sensible and comprehensible ways that simplify interaction and interpreting."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A useful data structure in python (which exists in other programming languages) is the **array**, part of the `numpy` library."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Arrays are **containers** of variables which can be accessed individually, looped over, or altered by functions. Examples of how to use them are shown below - experiment with different values and functionalities!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Printing arrays\n",
      "[1 2 3 4]\n",
      "[5 6 7 8]\n",
      "\n",
      "Simple array operations\n",
      "array_1 * array_2 =  [ 5 12 21 32]\n",
      "array_1 + array_2 =  [ 6  8 10 12]\n",
      "array_1 - array_2 =  [-4 -4 -4 -4]\n",
      "array_1 / array_2 =  [0.2        0.33333333 0.42857143 0.5       ]\n",
      "\n",
      "Indexing arrays\n",
      "array_1[0] =  1\n",
      "array_1[1] =  2\n",
      "array_1[2] =  3\n",
      "array_1[3] =  4\n"
     ]
    }
   ],
   "source": [
    "## Example 1: Simple array manipulations\n",
    "import numpy as np\n",
    "\n",
    "# Construct arrays of numbers\n",
    "array_1 = np.array([1,2,3,4])\n",
    "array_2 = np.array([5,6,7,8])\n",
    "\n",
    "print(\"Printing arrays\")\n",
    "print(array_1)\n",
    "print(array_2)\n",
    "\n",
    "# Operations for arrays\n",
    "print(\"\\nSimple array operations\")\n",
    "print(\"array_1 * array_2 = \", array_1 * array_2) # multiplies each element of array_1 by the corresponding element of array_2\n",
    "print(\"array_1 + array_2 = \", array_1 + array_2) # adds each element of array_1 to the corresponding element of array_2\n",
    "print(\"array_1 - array_2 = \", array_1 - array_2) # subtracts from each element of array_1 the corresponding element of array_2\n",
    "print(\"array_1 / array_2 = \", array_1 / array_2) # divides each element of array_1 by the corresponding element of array_2\n",
    "\n",
    "# Indexing for access to specific elements\n",
    "# The first element of the array is '0', the second is '1', ...\n",
    "print(\"\\nIndexing arrays\")\n",
    "print(\"array_1[0] = \", array_1[0])\n",
    "print(\"array_1[1] = \", array_1[1])\n",
    "print(\"array_1[2] = \", array_1[2])\n",
    "print(\"array_1[3] = \", array_1[3])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Functions can be used to modify components of arrays in many ways:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "test array =  [1 2 3 4]\n",
      "transformed array =  [ 0  2  6 12]\n"
     ]
    }
   ],
   "source": [
    "## Example 2: The following function\n",
    "# transforms an array by multiplying\n",
    "# each element by its index\n",
    "import numpy as np\n",
    "\n",
    "def transform_array(array):\n",
    "    \n",
    "    # enumerate gives access to index\n",
    "    # and elements of a container\n",
    "    for idx, num in enumerate(array):\n",
    "        # inline multiplication multiplies\n",
    "        # the element by the index\n",
    "        array[idx] *= idx\n",
    "    \n",
    "    return array\n",
    "\n",
    "test_array = np.array([1,2,3,4])\n",
    "print(\"test array = \", test_array)\n",
    "print(\"transformed array = \", transform_array(test_array))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Arrays can also be **multi-dimensional**!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Multi-dimensional array operations\n",
      "multi_array_1 = \n",
      " [[1 2]\n",
      " [3 4]]\n",
      "\n",
      "multi_array_2 = \n",
      " [[5 6]\n",
      " [7 8]]\n",
      "\n",
      "multi_array_1 + multi_array_2 = \n",
      " [[ 6  8]\n",
      " [10 12]]\n",
      "\n",
      "---\n",
      "\n",
      "Indexing multi-dimensional arrays\n",
      "multi_array_1[0,0] = \n",
      " 1\n",
      "\n",
      "multi_array_1[0,1] = \n",
      " 2\n",
      "\n",
      "multi_array_1[1,0] = \n",
      " 3\n",
      "\n",
      "multi_array_1[1,1] = \n",
      " 4\n",
      "\n",
      "---\n",
      "\n",
      "Shape of multi_array_1 =  (2, 2)\n",
      "\n",
      "Shape of multi_array_2 =  (2, 2)\n"
     ]
    }
   ],
   "source": [
    "## Example 2: Multi-dimensional arrays\n",
    "import numpy as np\n",
    "\n",
    "multi_array_1 = np.array([[1,2],[3,4]]) # This forms a 2x2 matrix!\n",
    "multi_array_2 = np.array([[5,6],[7,8]])\n",
    "\n",
    "# Multi-dimensional array operations:\n",
    "print(\"Multi-dimensional array operations\")\n",
    "print(\"multi_array_1 = \\n\", multi_array_1)\n",
    "print(\"\\nmulti_array_2 = \\n\", multi_array_2)\n",
    "print(\"\\nmulti_array_1 + multi_array_2 = \\n\", multi_array_1 + multi_array_2)\n",
    "\n",
    "# Indexing is more complicated:\n",
    "print(\"\\n---\\n\\nIndexing multi-dimensional arrays\")\n",
    "print(\"multi_array_1[0,0] = \\n\", multi_array_1[0,0])\n",
    "print(\"\\nmulti_array_1[0,1] = \\n\", multi_array_1[0,1])\n",
    "print(\"\\nmulti_array_1[1,0] = \\n\", multi_array_1[1,0])\n",
    "print(\"\\nmulti_array_1[1,1] = \\n\", multi_array_1[1,1])\n",
    "\n",
    "# The 'shape' function tells you the dimensions of the array\n",
    "print(\"\\n---\\n\\nShape of multi_array_1 = \", np.shape(multi_array_1)) # 2x2 matrix\n",
    "print(\"\\nShape of multi_array_2 = \", np.shape(multi_array_2)) # 2x2 matrix\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "___\n",
    "**Exercise 2a**: Array transformations\n",
    "\n",
    "You are tasked to work with data to produce a histogram. For each bin the data is provided in the following format:\n",
    "\n",
    "$$[\\mathrm{ \\tt x\\_low \\quad x\\_high \\quad value \\quad error\\_minus \\quad error\\_plus}],$$\n",
    "\n",
    "where $\\mathrm{\\tt x\\_low, x\\_high}$ are the edges of the bins in $x$, $\\mathrm{\\tt value}$ is the value of the histogram at that point, and $\\mathrm{\\tt error\\_minus, error\\_plus}$ are the **statistical** error bars in $y$ (i.e. the total uncertainty on $y=\\mathrm{\\tt value}$ is $\\mathrm{\\tt error\\_minus + error\\_plus}$).\n",
    "\n",
    "Produce a function that can add two histogram bins such that the **values** are added **linearly**, and the **statistical errors** are combined **in quadrature** as discussed in the **introduction** notebook.\n",
    "\n",
    "**NOTE**: To combine two different contributions to the same bin, the $\\mathrm{\\tt x\\_low}$ and $\\mathrm{\\tt x\\_high}$ entries **MUST** match."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "None\n"
     ]
    }
   ],
   "source": [
    "# Define function to physically combine\n",
    "# two entries for the same histogram\n",
    "# bin in the stated format\n",
    "import numpy as np\n",
    "\n",
    "def combine_bin_contribs(bin_1, bin_2):\n",
    "    \n",
    "    # Hints:\n",
    "    # Consider how each component needs to\n",
    "    # transform when you add contributions\n",
    "    # to histogram bins.\n",
    "    \n",
    "    # should the [0] and [1] components\n",
    "    # change?\n",
    "    \n",
    "    # the values [2] should be combined\n",
    "    # by normal addition.\n",
    "    \n",
    "    # the statistical errors, [3] and [4],\n",
    "    # should be combined in quadrature.\n",
    "    \n",
    "    return # Return value here\n",
    "\n",
    "if __name__ == \"\"\"__main__\"\"\":\n",
    "    \n",
    "    contrib_1 = np.array([5, 10, 20, 3, 3])\n",
    "    contrib_2 = np.array([5, 10, 10, 4, 4])\n",
    "    print(combine_bin_contribs(contrib_1, contrib_2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "___"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reading external files"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Data from experiments and from theoretical predictions can be imported in a variety of formats, often from external files"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We have included data in a format similar to the one from Exercise 2a, in the `.dat` files in the same folder as this notebook.\n",
    "\n",
    "We can read these in python in a number of different ways:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0.000000e+00\t5.000000e+01\t6.477501e-06\t8.500424e-08\t8.500424e-08\n",
      "5.000000e+01\t1.000000e+02\t2.198127e-05\t3.679400e-07\t3.679400e-07\n",
      "1.000000e+02\t1.500000e+02\t7.112714e-06\t1.320287e-07\t1.320287e-07\n",
      "1.500000e+02\t2.000000e+02\t7.348822e-06\t1.373821e-07\t1.373821e-07\n",
      "2.000000e+02\t2.500000e+02\t4.699381e-06\t1.578399e-07\t1.578399e-07\n",
      "2.500000e+02\t3.000000e+02\t6.141894e-06\t3.886073e-07\t3.886073e-07\n",
      "3.000000e+02\t3.500000e+02\t4.109160e-06\t4.575637e-07\t4.575637e-07\n",
      "3.500000e+02\t4.000000e+02\t3.209164e-06\t9.512975e-08\t9.512975e-08\n",
      "4.000000e+02\t4.500000e+02\t3.287922e-06\t9.084217e-08\t9.084217e-08\n",
      "4.500000e+02\t5.000000e+02\t2.588623e-06\t1.335026e-07\t1.335026e-07\n",
      "5.000000e+02\t5.500000e+02\t2.629022e-06\t1.139008e-07\t1.139008e-07\n",
      "5.500000e+02\t6.000000e+02\t2.524809e-06\t8.615713e-08\t8.615713e-08\n",
      "6.000000e+02\t6.500000e+02\t2.962798e-06\t1.618244e-07\t1.618244e-07\n",
      "6.500000e+02\t7.000000e+02\t1.694864e-06\t9.617141e-08\t9.617141e-08\n",
      "7.000000e+02\t7.500000e+02\t2.198500e-06\t1.947348e-07\t1.947348e-07\n",
      "7.500000e+02\t8.000000e+02\t1.848204e-06\t9.786827e-08\t9.786827e-08\n",
      "8.000000e+02\t8.500000e+02\t2.190662e-06\t8.329120e-08\t8.329120e-08\n",
      "8.500000e+02\t9.000000e+02\t3.398384e-06\t3.292098e-07\t3.292098e-07\n",
      "9.000000e+02\t9.500000e+02\t1.981245e-06\t6.649841e-08\t6.649841e-08\n",
      "9.500000e+02\t1.000000e+03\t1.064835e-06\t9.814275e-08\t9.814275e-08\n",
      "<class 'str'> \n",
      "\n",
      "\n",
      "[[0.000000e+00 5.000000e+01 6.477501e-06 8.500424e-08 8.500424e-08]\n",
      " [5.000000e+01 1.000000e+02 2.198127e-05 3.679400e-07 3.679400e-07]\n",
      " [1.000000e+02 1.500000e+02 7.112714e-06 1.320287e-07 1.320287e-07]\n",
      " [1.500000e+02 2.000000e+02 7.348822e-06 1.373821e-07 1.373821e-07]\n",
      " [2.000000e+02 2.500000e+02 4.699381e-06 1.578399e-07 1.578399e-07]\n",
      " [2.500000e+02 3.000000e+02 6.141894e-06 3.886073e-07 3.886073e-07]\n",
      " [3.000000e+02 3.500000e+02 4.109160e-06 4.575637e-07 4.575637e-07]\n",
      " [3.500000e+02 4.000000e+02 3.209164e-06 9.512975e-08 9.512975e-08]\n",
      " [4.000000e+02 4.500000e+02 3.287922e-06 9.084217e-08 9.084217e-08]\n",
      " [4.500000e+02 5.000000e+02 2.588623e-06 1.335026e-07 1.335026e-07]\n",
      " [5.000000e+02 5.500000e+02 2.629022e-06 1.139008e-07 1.139008e-07]\n",
      " [5.500000e+02 6.000000e+02 2.524809e-06 8.615713e-08 8.615713e-08]\n",
      " [6.000000e+02 6.500000e+02 2.962798e-06 1.618244e-07 1.618244e-07]\n",
      " [6.500000e+02 7.000000e+02 1.694864e-06 9.617141e-08 9.617141e-08]\n",
      " [7.000000e+02 7.500000e+02 2.198500e-06 1.947348e-07 1.947348e-07]\n",
      " [7.500000e+02 8.000000e+02 1.848204e-06 9.786827e-08 9.786827e-08]\n",
      " [8.000000e+02 8.500000e+02 2.190662e-06 8.329120e-08 8.329120e-08]\n",
      " [8.500000e+02 9.000000e+02 3.398384e-06 3.292098e-07 3.292098e-07]\n",
      " [9.000000e+02 9.500000e+02 1.981245e-06 6.649841e-08 6.649841e-08]\n",
      " [9.500000e+02 1.000000e+03 1.064835e-06 9.814275e-08 9.814275e-08]]\n",
      "<class 'numpy.ndarray'>\n",
      "(20, 5)\n"
     ]
    }
   ],
   "source": [
    "## Example 3: Reading files\n",
    "import numpy as np\n",
    "\n",
    "# Native python open() function\n",
    "# 'r' argument means 'read-only'\n",
    "f = open(\"lhc-data.dat\", \"r\")\n",
    "print(f.read())\n",
    "print(type(f.read()),\"\\n\\n\") # The native python 'open' function converts the file read to a string - not a useful format\n",
    "\n",
    "# Try with numpy - which has some smarter functions!\n",
    "example_histogram = np.genfromtxt(\"lhc-data.dat\", dtype=float)\n",
    "print(example_histogram)\n",
    "print(type(example_histogram)) # The method has converted the data to a numpy array!\n",
    "print(np.shape(example_histogram)) # The dimensions are right too - 20 bins, 5 columns for each bin!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "___\n",
    "**Exercise 2b**: Complex array transformations\n",
    "\n",
    "Using the solution to Exercise 2a (i.e. not writing a new function from scratch), write a function that will add two histograms together."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define function to physically combine\n",
    "# two histograms, using the function(s)\n",
    "# you have created earlier.\n",
    "import numpy as np\n",
    "\n",
    "def combine_histograms(hist_1, hist_2):\n",
    "    \n",
    "    # Hints:\n",
    "    # Can you use some of the tools\n",
    "    # we introduced in the previous\n",
    "    # notebook to enable the use of\n",
    "    # the function from Exercise 2a?\n",
    "    \n",
    "    return # Return combined histogram here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "___\n",
    "### Combining theoretical predictions\n",
    "\n",
    "We have provided theoretical predictions for Higgs production in the base folder - including predictions from *High Energy Jets* ([HEJ](https://hej.hepforge.org/)) for the **strong-initiated process**, and predictions for *Vector Boson Fusion* or *VBF* (and related) which is the **weak-initiated** process.\n",
    "\n",
    "The composite prediction for Higgs production for this analysis will be the combination of both processes - you will need to **combine the histograms** for each component to produce a reasonable estimate of the Standard Model background.\n",
    "\n",
    "For each theoretical prediction we have provided three data files, one indicating the central prediction, one indicating the lower theoretical bound, and the other indicating the upper theoretical bound.\n",
    "\n",
    "For HEJ, these are `hej-prediction.dat`, `hej-prediction-lower.dat`, `hej-prediction-upper.dat` respectively.\n",
    "\n",
    "For VBF, these are `vbf-prediction.dat`, `vbf-prediction-lower.dat`, `vbf-prediction-upper.dat` respectively."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "___\n",
    "**Exercise 2c**: Reading files and combining predictions\n",
    "\n",
    "Using the solution to Exercise 2c, and the example for reading files, create three composite histograms by combining the HEJ and VBF predictions:\n",
    "\n",
    "1. Add the **central** predictions into one histogram for the composite central prediction (`hej-prediction.dat + vbf-prediction.dat`\n",
    "\n",
    "2. Add the **lower bound** predictions into one histogram for the composite lower bound prediction (`hej-prediction-lower.dat + vbf-prediction-lower.dat`\n",
    "\n",
    "3. Add the **upper bound** predictions into one histogram for the composite upper bound prediction (`hej-prediction-upper.dat + vbf-prediction-upper.dat`\n",
    "\n",
    "This will give you total Standard Model predictions and an upper and lower **theoretical** bound."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Produce the composite histograms \n",
    "# using the tools you have developed/used.\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "___"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Plotting revisited"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Congratulations! You should have combined the theoretical predictions to produce a composite histogram (don't worry if not, the files `sm-prediction.dat`, `sm-prediction-lower.dat`, and `sm-prediction-upper.dat` are pre-made and available for you to use instead).\n",
    "\n",
    "Now you need to plot the results with the experimental data from `lhc-data.dat`.\n",
    "\n",
    "Remember the guidance from the introduction of what goes into a good plot!\n",
    "\n",
    "For the histogram, we require two lines:\n",
    "\n",
    "1. A scatter plot of the experimental data with **statistical uncertainty** plotted as **vertical error bars**.\n",
    "\n",
    "2. A composite Standard Model theoretical prediction with **statistical uncertainties** plotted as **vertical error bars** (from $\\mathrm{\\tt error\\_minus, error\\_plus}$ ), and **theoretical uncertainties** plotted as **shaded band** between the upper, lower and central predictions.\n",
    "\n",
    "Here is the example plot again for guidance:\n",
    "<img src=\"../introduction_notebook/example-distribution.png\" alt=\"Example distribution for Higgs production\" width=\"500\"/>\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "___\n",
    "**Exercise 2d**: Producing the plot!\n",
    "\n",
    "Produce the required plot described above, pay attention to the special guidance from the introduction notebook.\n",
    "\n",
    "You can use the previous variables and functions you have created/used earlier.\n",
    "\n",
    "Some useful links for matplotlib:\n",
    "- [Plotting histograms](https://matplotlib.org/stable/gallery/statistics/histogram_histtypes.html#sphx-glr-gallery-statistics-histogram-histtypes-py)\n",
    "- [Shading regions between lines](https://matplotlib.org/stable/gallery/lines_bars_and_markers/fill_between_demo.html#sphx-glr-gallery-lines-bars-and-markers-fill-between-demo-py)\n",
    "- [Shading regions between lines](https://matplotlib.org/stable/gallery/lines_bars_and_markers/fill_between_demo.html#sphx-glr-gallery-lines-bars-and-markers-fill-between-demo-py)\n",
    "- [Error bars and scatters](https://matplotlib.org/stable/gallery/lines_bars_and_markers/errorbar_limits_simple.html#sphx-glr-gallery-lines-bars-and-markers-errorbar-limits-simple-py)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAZAAAAEGCAYAAABLgMOSAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADh0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uMy4yLjIsIGh0dHA6Ly9tYXRwbG90bGliLm9yZy+WH4yJAAAgAElEQVR4nO3dfWxU95kv8O9jmBhMjF95STAFj4G71O22GYYmLKuEkHGuxC40mxhCtEp3pW3sNpVWqraLm1x1e1eRFuFu7uaPbbo2u1pp0W4b4kRNULMSnkCiTaFJzIRIUBIC46SYxBTbjDEYg7F/9485Zzwez8uZc868nDnfj4QyPi+/eXwC88zvXZRSICIiylZZoQMgIiJnYgIhIiJTmECIiMgUJhAiIjKFCYSIiExhAiEiIlPmFzoAO9TX16vVq1cXOgwiIsc4ceLEkFJqiZUySiKBrF69Gn19fYUOg4jIMUTkM6tlsAmLiIhMYQIhIiJTmECIiMiUkugDISLrJicnMTAwgImJiUKHQjZasGABGhoa4PF4bC+bCYSIAAADAwOorKzE6tWrISKFDodsoJTC8PAwBgYG0NjYaHv5bMIiIgDAxMQE6urqmDxKiIigrq4uZ7VKJhAiiil08ujs7EQwGERPTw86Oztjx3t6eiyX3dHRMatMK/eFQiG0tLTMOR4MBtHU1IRQKGQ5pmAwiJaWlqzjTZTL/6dMIERUFHp6euDz+RAIBNDa2ho7HolE0Nvba7n8xx9/3Lb7fD4fOjo68NJLL8055/V64fP5LMcUCARQXV1tPNACYB8IgN+//TYmr161VIZn8WIsfeABmyIich+v14uOjg54vV54vV60tbUBAMLhMPr6+hAMBhEIBABEv52Hw2F4vV4EAgEEg0Hs27cPHR0dCIVCCAQCsQ/xzs5O+Hy+WbWCVPe3t7cjHA5jz549Se9LFAgEEAqF4PP5EIlE4PV6Z53v7u6G3+9HX19f7PdJFVMkEkF3dzd8Ph/C4fCs64uVo2sgIrJdRLpHR0ctlTN59SrKlyyx9MdqAiJyO5/Ph507d2Lnzp1oampCOByOHdc/6IFoQtE/YPft2wcg+kEeDodjtRe9ZqB/IAcCAUP3t7a2Ys+ePUnvS6a9vR1dXV2xcmtra2PnOjs74ff7Y/F3d3enjAkA9u7dGzt24sQJW55prjm6BqKUOgTgkN/vf6rQsRCVmlPPPTfnWP1992F5Swumbt7EmSRt90vvvx9LH3gAk2Nj+PiFF2ad+8qPfpT2/SKRCNra2tDW1oZIJIKdO3cmbbrSayeRSGTW8WTNRr29vbEkkc39ye5Lxuv1xhJdsvfWm+K8Xi+6urrQ1taWsmy9XyUUCqG9vT3jexcDR9dAiKh0HDx4MPahXl1dnbT9PxQKIRQKGe4M37hxY6yZaGRkJFZGpvuT3ZfKzp070dHRMSeB6U1RQLR2snHjxrRl6x3meo3FCRxdAyGi3ElXY5hXXp72vKeyMmONI5lgMIjq6mqEw+FZ38K9Xi+CwSD8fv+sa7xeL3p6euD1ehEKhRAOhxEMBhEKhRCJRGJ9GXri6e3txdKlS+fcX11dHbvG5/Mlva+trS2W1EKhEPbt24fa2lrs2rULe/fuBRBNguFwOHZeT1ShUAh79uwBgJRl68d1tbW1sd+pWBOKKKUKHYNlfr9fWVmN9+KhQyhfYmlVY9y8fBkrtm+3VAZRIZ05cwbr168vdBiUA8n+34rICaWU30q5bMIiIiJTmECIiMgUJhAiIjKFCYSIiExhAiEiIlNcP4z30pEjuPDqq5hXUQEAWLV7N8o8Hoz09WH0zJk51zc++SQAYOj4cYydOxc7vmjlSo7CIiJXcX0N5PKvf42bQ0OWypgYHMTVjz+2KSIi59iyZQu2bNlS6DCoQFxfAwGA8vr6WM1CV+v3o9afeoh0/aZNqN+0CQDw2c9/jqnx8ZzGSFTqQqEQnnrqKTz++OOxxQn37t1r+7pQ4XAYHR0dePnll20tt6enZ9Yqwon0BRsTl2eJ/729Xm9sdnqmxRQzvV8+uL4GsvT++7HY4uSpVU88gRU7dtgUEZE7xS+aqC+KuH///jlrVlnl9XptTx5GlpxPtTx7/O/d2toaSxz64otm3y8fmEAeeABVnH1LVHT0/UGqq6vR09ODmpqa2CKL3d3dCAaD2LBhA4LBYOxnIPrhqm9MpR9vaWmJbVKlL1oIzGzaFAwG0dnZiZ6eHgSDQbS3tyMSiaQsS79eX9Mqfsl5XWJc2di1a9esBRcTy7L7/cxyfRPW5NgYpm7csFTG5f/5H0xevcpOdCp5if0dJ0+eTHr8rbfeMv0ewWAQiUsTtba2YmRkBAcPHkR7e3tsGfTa2trY66amJpw/fx579+6NNYO1t7ejra0N7e3ts5p79JpAIBCIlef1etHe3o7e3t7YB3Rvb2/SsvTru7q64PP50i4539LSknZJ+GT0tbpSlWX3+5nl+hrIxy+8gM//+78tlXHt008xPjBgU0RE7hYIBNDW1hb7ENS/5be1taGrqwv+uL7J+CYhfWn1UCiEkZGRWcuip9shMP6c/lrf1yPbsuJjSbZkvFHxm1MZKcvq+5nl+hoIAPzq2DEc+fDDlOdPffIJAOAra9cmPf/QjRvYdt99OYmNqJgk1iz0moeVGkcqXq8XkUhk1jLs+/fvR0dHR2wTp/gPTH3V2vhl0a1+oJopS094wWDQ9K6CBw8eREdHR6y8dGXZ8X5mub4GQkTFQV/ePBgMIhgMoqenBw899FBsyXV9z41wOBxb9lyvHXR3d8eSyp49e2Ll6P0Eetnx76PXVhKXgQ+Hw+jt7UVvb++cspJdrycWfcl5vSaUuGR8/L2Jv7dent6PAWDWlr6JZRl5v3xw/XLup557DuMXLljqv7jw6qso83jwjTSjJoiKnZnl3HNZAzFi586dto+oKkW5Ws6dTVgAxOOxlEDGzp3D+MWLNkZERJnE1xiKdcOlUuf6BLI8EMDob39rqYw/+P73MfSb39gUEZFzFKrmAUQ728+fP1+w9ycmkOhscpFCh0FE5DiO7kQXke0i0j06Omq6jJtDQ5i0cD8AfPaLX+DSkSOWyiAqBqXQJ0qz5fL/qaMTiFLqkFKqraqqynQZn/zsZxh4/XVLcYx98gn7QMjxFixYgOHhYSaREqKUwvDwMBYsWJCT8l3fhEVEUQ0NDRgYGMDly5cLHQrZaMGCBWhoaMhJ2UwgRAQA8Hg8aGxsLHQY5CCObsIiIqLCYQ3EBnfU1uL29euFDoOIKK9cn0Du3rYNo6dPWypj3fe+x3kgROQ6rk8gtRs2YHpystBhEBE5juv7QG58/jluDg9bKqP/P/4DXxw+bFNERETO4PoEcv7f/g2fv/GGpTKuf/YZJi5dsikiIiJncH0CISIic5hAiIjIFCYQIiIyxfWjsOyw8K67MH3rVqHDICLKK9cnkIZHHkHk1ClLZTR9+9ucB0JEruP6BFL91a9yFjkRkQmu7wO5/umnuDE4aKmM8//6r/j8V7+yKSIiImdwfQLpP3AAg729lsq48cUXuDkyYlNERETO4PoEQkRE5jCBEBGRKUwgRERkiutHYdlh0apV3EeaiFynJBLI7bExXDx0yNS9C5Ytw6LVqy29f+O3vsV5IETkOiWRQNT0NMqXLDF1b/mSJbh5+bLNERERlb6SSCBWjA8M4JbFIbhnf/pTjF+4gPr77rMpKiKi4uf6TvRLR49i6PhxS2XcGhnB5NiYTRERETlD2hqIiNwDwA8gVQ+xaOf6lFInbY6NiIiKWKYmrFoALymlrqa7SEQeA+DqBKImJ0135Os8ixdj6QMP2BQREVFupU0gSqk3ReRnAL6b4bpXbI3KgaYnJ0135OvYmU9ETmKkEz0sIlsB1AA4z6aquSrXrsXUjRuFDoOIKK8yJhCl1E/01yJSJSKPaj8GMzVtOcHylhbLo7BW7d6N+YsW2RQREZEzZDuMtwbANwC0AuhFhqYtJ1i4fDnK5s0rdBhERI6TMYGIyLe1l99BdMRVl1JqTU6jyqNr/f2YvHLFUhkf/dM/YezsWTT+xV/YFBURUfEzUgPpBNAFYKdSqj/H8eTd5XfewdT4ONZaKOP2tWuYmpiwLSYiIicwMpFwp1LqmXwnDxHxisjLItKWz/clIiJjjHSivwkAIvIUAB+itZF+ABuUUkdyGx6eUkpFcvweRERkQjZLmZxXSn0XgCilRo3cICKtItKb5FjAQM1iBIBXu96bRZxERJQH2SSQDSLydQA12rwQX6YblFI98T+LSKt2PKj9HNCPJ/ypBuBXSoUAhBEd9VW0qpqbUdHQUOgwiIjyKpthvN0AnkE0cRxWSv2jiffbCOAl7XVYKyuYmGgAQETCIuJDdC2ubhPvZcjd27bh1vCwpTJWPvooyjwemyIiInIGwwlEa7b6oYhUGW3CSqI64ee6NO8X1l6GTL6XIeV1dcD0dC7fgoioJGVswhKRvxWRn2nNVwAQ1H7+gYn3iyC6QKNlItImIn0i0jc8ajafAWNnz+Jav7UBZr/dtw8XX3/dUhlERE5jpAYSAtATN4z3TURnoWfsA0nifczUQrxaOaYopbqhNW19be1a0xuSD737LqbGx83eDgCYvnUL07dvWyqDiMhpjHSiq4Q5IF3a0N5wqht0Wie5P67zvAfRkVUBANV6ZzoRETmPkRrIrH6LuGSS8Vu/liBqEo51ai+ZPIiIHMxIDaRJRFbHH9B+brI/HCIicgojNZBuAC+LiEK0E9wLIKyUejynkTlIzT33QHEkFxG5jJGlTEYBPCwiDyGaPPqUUh/kPDIDRGQ7gO2r77rLdBkNO3bgpsV5ICv+9E8BZbofP2ZqYoLb4hKRYxhZzv3rSqmTWsf5m3mIyTCl1CEAh762du1TZsvwVFVh+tYtG6Myr2LlSstlcFtcIsoXI01Yoi2kqAAcLIVdCOONnj6NSQvzSADg1HPP4fI77+DONTPbpFStX49avx/Tk5P47Be/mHNP9R/+IWq+9jXcHh/HhVeiW8pXNzejxmdmdDQRUf4ZacL6AMAHACAij4lIDaJ9ILleiTcvRkIhy/NAlmzejGvhjKOa05oYHEQEYAIhIsfIaktbpdQrACAijXG1kqBS6tMcxOYYy7Zuxe3r11G+ZMmcc2UeDxqffDLlvfMrKtD45JPoP3AglyESEdku2z3RAcTmguwHAK1z/VMbYyIiIgfIOoGIyOr4Goe+4RRZU8umKyJyGMMJRKtp7AMwLCIRRJc0KWg/iD6M17t48ZwmIKOd2AvvugvXzp/PU8SpVTU3FzoEIqKsZFMDqVZK+fUfROSeHMSTFX0Y7/r6etPDeOv/6I9QNt9US56t9JFgnqqqAkdCRGRMNp+cVzL8XDB31NSk7Kg20oldDN/+B7Tl4NPFSkRUTIxMJDyI6GirJhEZBjCK6AKL5wF8N7fhERFRsTJSA+liR3l+HDpyBG8eP5703KlPPgEAfGXt2rRlHPz7v7c9LiKiZIxMJJyVPBJHYVFU67PPpt0XPVMCeOjGjZzERUSUK9mOwuoCEBaRRgDthR6FVWq2b92Kv7bYBzJ+4YKlBRm5GCMRGZVNJ7pXKRVb7EmbiV4Uw3itrMZrl55/+IekM9F133z6aQDAay++mPT82NmztsRhdUFGLsZIREZlk0ASJ0u8DxS2ScuO1XjzJVXi0FWuW5enSIiI7JFNAunURmFFEN2mtkZE+gHcAyB9zy5ldHN4GEPHjuFWJDLr+JI//mPc2diIG4ODGOztnXPfsgcfREVDA8YHBnDp6FGu6EtEeZNNAulINhpL6xshi8Y/+2xO8sgWV/QlonxKm0D0zaSA1GteKaXejL+uEM4PDMT6GBIZGf5aDENfa3y+tB/8C5cvTzvJsKKhAQuWL89FaLNs2bIFAPDWW2/l/L2IqLhlqoE0iYg/7mdJco0CMAKgYAmEiIjyL20C0ff/KHZNDQ0ZO6nTsWPkkWfxYsvlTE1M2LKtLRFRPhR+FcESYcfcCSvzNwBgeUuL5Rhan30W5c8/n/L8yZPRiqbelJUMm7eI3IEJpIQszEMfCBGRztEJpJgmEhaDa/39AIA7GxtNl/HS3/0d5i1YkPJ867PPAgD+82/+JuU1v3/7bc5mJ3IBRycQJ00kzIfL77yDicHBWaOx7t62DeV1dRg7exZD7747556GHTvgqarC6OnTGAmFMs4j0df7SjfrnrPZidzB0QmEZqtuboaVmSScR0JE2WACKSHp5pJUrluXdrmUquZmjIRCGd/Dymg3IiotZYUOgIiInIkJhIiITGETVhEp9GTElY89Zum9ichdmECKSKEnI86vqLD8/kTkHkwgFHPlww8xdOwY5t9556zjq3bvRpnHg5G+PoyeOTPnPn2Rx6HjxzF27hwWLltmORbujEhU/JhAaMbU1Jzkka2JwUEAwPI080SM4FwSouJnKoFo29l6AfQCaFFKPWNrVFQQmZaUr/X7Uev3pzxfv2kTxs6dy0VoRFSEzNZAwkqp/SJyD6K7ExYElzKZy2pHPFcEJiKjzCaQ89omUh+ISJetEWWBS5nMla7fwMhmUFZXBC6bz1ZRIrcw/K9d33VQRP4WQJ12zAsg8/TlHJOyMkvfuj2LF9sYTWGlW2bdyFLs6RZJNGLVE09Yup+InMNwAonbsjYEoE8pNao1YbWIyLBS6tWcRGjA/MpKrNi+vVBvT0TkSlm3N8Tvja6U+gDAB1qthIpAuuYpI01Yv3/7bUu1ueH33sNIXx8Web2xY/MWLsSXWlsBAJeOHMH4xYuz7vFUVqLhkUcAAF8cPoyJS5ewaOVKfikgKnKWGqy10Vi9AIL2hEO5ZGSnQKtzL+YvWoTxgYGU53/6X/+F+qkpbN+6NeU1E4ODuD02Zrk/hnNJiHLLUgJRSu3XXn5qPRQqBcu2bsXt69dT7hfy4R13AAD+Wpt8mOiuhx/GxKVLANLvOWIE55IQ5ZblxRRFZLX1MIiIyGnMTiT8FwDnAAiiEwq/a2dQ5G7ltbWFDoGIDDDbhNWhlBq1NRIqGekmM05PTgJI37xU941vYGpiIiexEZF9zM4DqRURAHgfQEgp9WmO4iMH2vXjH6c8d+bCBQDAnz//fNoy/p/Ph6mETv8Fy5bhrocfBgAM/PKXmBwbm3W+YsUKLNM653/X04MF9fUcyUWUQyUxD4Qo0dhHH+GWxU54IkrP0fNA9LWw1qxZU4i3pxSszkUBokuqpBuFpc8bSaVi1SrLQ4E5DJgoPbOd6Kvjm62UUj+xLaIs6Gth+f1+roVFc8yvrLQ0FJjDgInSMzyMV0QeE5F/EZFvaz8/mruwiIio2GVTA4kopb4jIg8B+A6AoRzFRGR5Wfry2trYiC8iyo1sEsgwEOsDeTPDtUSWWO17WLF9Oy4eOoTPfv5zTN++Petc5Zo1qN+0CQDQf+DAnHur1q9Pu3EWUTpG+/lKQTYJZLeIdAK4gujw3WDcyCyikjI9OTkn8RDRbNkkkJeUUj8EAG34bhMAJhDKSr6/lWXan6QxxZpc/QcOYGp8HCv/7M9yERZRScgmgdTokwn14bu5CoqoGKipKa4ITJRGNglkA4A6EXkWgALwvlLqH3MTFlHhWR0GDHAoMJW2bBJIEIhNHoSINOYkIiKbWBnJNTU+DjU1ZXNEVArSbQkNGNs6ulQ62LNZyiSxyWrY5liIbJWu6SjTSJkrJ09i9NQpAMDt8XFceOWVOdfU+nyoam7G5OgoBl5/fc75+nvvxR01NdkHTuQQGROIiPwg2WEAAQD/2/aIiDIwOkwy3TfATN8SvTdu4AePPWYiOip1Rv/elUotIx0jNZB6AC8hmjD0rWu5YQPllJUPfzuEFy5E1fr1AID5FRUpR2sBgKeqKun5m8PDuDXMijqVrowJJG7obk18M5aI8F8GFTWr3wCtjsD6/I03MDU+jsZvfctSOUTFKqtRWNoeIGEAPkR3IuQ8EMoJO1b0tcrqcipT4+MArCciDgWmYpVNJ/pPROQpRNfBeo9DeKlQ8tW2bPVD+8rJk7gWDhf9UGA3tdmTvYx0on9dX7JEKbUfwP6cR0VE5FBuSsRGaiCi1TwUgINKqas5jskwbihFxU7Kojsm3BgcxGBv75zzyx58EBUNDRgfGMClo0fnnF/e0oKyefMsx2F1UIKbPhTJOCOd6LFlS7Q9QWoAhJVSR3IdXCbcUIqK2ZLNm1FeV1foMIhyRpRS2d8UnYUeQLRWEozfnbAQ/H6/6uvrK2QIREll2po3k2v9/Zi8cgVrn37axqhmYx+IO4nICaWUpX0LTG1pq5Tqh9YXom0w9amVIIhKldWRXIOHDwMAKjiSi4qQ4QQiIluTNVtpG0wRURJOGMnFmgeZlU0NpF1EqhHt/+D8D3I9q00/Ru+/OTSE/gMHcPe2bSivq8PY2bMYevfdOdc17NgBT1UVRk+fxkgoNOvcopUrsWL7dlNxEqWSTQJpQXRJk40i0g5gX6H7PoiKndXRT94bN9D25S9bimFicDA2qZHITtkkkG6l1Kv6DyLyKNj3QSXMjmW7rQovXIiVjz46qwmrct06VK5bl/KequZmVDU3x36+PT6OW0NDOYuR3CubBNIrInsBdGk1D45PJMrAjiVZ0i2F8k1tdNZrL76Y8pr5FRWYWrgw7XsQmZHNUiZvikgYwE4RqQXQlbuwiAqvWJbt3vWjH2E6xeZWp8NhAMD2p1JPhfJOTeH//OVfWoqBo7gomayG8WrDd38CACKyOCcREbmE0cTjqa5Oea7swgUASDthce2VK7h65gyW3n9/VvHF49a8lEzGiYQpNpQCgBalVFFsKMWJhORWRmpBp557DsPvvYeKL30JALBq926UeTwY6evD6Jkzc67X9zYZOn4cY+fOAYiO4vryD39ob/BUUPmaSJhqQymx8sZEZJ2RWsySzZtxTWvqMoOjuCgVbihFVOKWbd2K29evz5mMWOv3o9af+gto/aZNqN+0Cf0HDuQ8gXA5FWfihlJElFbZ/PlQ802tekQljhtKEbmAlTW5lgcCALizIs2V7SgsbihF5EBWP7itrioMOGMkF5vSssN6KRGldeHVV3Hlgw9w95/8iaVyWp99FuXPP5/0nNFZ/XZ8sHNzLfswgRBRWqOnT+PKyZO4OTICAJi3cCG+1NoKALh05AjGL16cdb2nshINjzwCAPji8GFMXLoEAFgzNYULeYybco8JhIjSsjoMGIgOBf7evffivn//96Tn89l0ZMfyMhTFBEJEaaUaBqyfS+euhx8GgLwMBbYDE0d2mECIKCOrOyvOKy/HvPLylCO5bg5Hp5VlGunFkVzFhQmEiDKy+qG9Yvv2tCO5yjweAMg40ssJI7msclIzmqMTiIhsB7B9zZo1hQ6FiDK4dPQopm7enHVswbJluOvhh/Haiy9i4Je/RP+BA7POV6xYEWsm+11PDxbU15fEzopWR4IBxZFgygodgBVKqUNKqbaqqqpCh0JEOTb20Ue4+vHHhQ6D4ji6BkJEzrHswQfTNlHpQ39TqVi1Km1HvJOafkplJBgTCBFREXFC4tAxgRBRXlgdyTU1Po5fHTuG73/1q0nP67szbkpxXvfaP/8zR3LZhAmEiPLC6of29OQkBn/zm9iIrURSFu3STXVeN3n1qqU4aAYTCBFZlo92+5WPPor/6/FgMBjE9O3bs85VrlmDv9JGcL2wadOce6vWr0et34/pyUkM/frXJTGSqxg4ehQWEVE2ho4d40guG7EGQkSOsuqJJ5Iefy1JzSPR9d/9zu5wXI0JhIgyyjSpjcuguxMTCBE5hh0juQBruytyPa4ZTCBElFGm2kO+Jr9Z/eC+cvIkroXDlnZXdMN6XEYxgRCRayy9/36UlZfj9vg4LrzyypzztT4fqpqbMTk6ioHXX59zvv7ee3FHTU0+QnUEJhAisswp/RtLH3iA80BsxARCRK4zv6ICjU8+mfK8p6oq6fmbw8O4pe1dQkwgRESGff7GG7g9Nma5GatUOuKZQIjIVayM5NJHcVnphAdKpyOeCYSIXMXKN399FBdFMYEQEWXh5tAQrvX3487GRtwYHMRgb++ca5Y9+CAqGhowPjCAS0ePzjm/aOXKkliPi2thEREZtGTzZpTX11sqY/6iRShfutSmiAqLNRAiIoOWbd2K29evx/pAFi5fnnY0V0VDQ/LRXOwDISKibF3r78fE4KCl5VSA4hjJxQRCRJQFq+txDR4+DDU1hXoDqwenUwy1GCYQIqIs2LUeVylgAiEiyrObQ0PoP3AAd2/bhvK6OoydPYuhd9+dc13Djh3wVFVh9PRpjIRCs84Vw0guJhAiojxasnmz5RrIxOBgbFJjITGBEBHlUeJILgCoXLcOlevWpbynqrkZVc3NsZ9vj4/j1tBQTuM0ggmEiMhh5ldU4JaIpZFci8rKFluOw2oBRESUHasjuUbPnMH0rVtYHgiYLmMeMM/0zRomECKiPLNjJNfwe+/hxhdfxI6t2r0bZR4PRvr6MHrmzJx79AmNQ8ePY+zcOUvvr+NSJkREDmPHkip2EKVUoWOwzO/3q76+vkKHQUSUNxcPHUq5rPw3n34aAPDaiy+mvL9p8+bw6NRUk5UYWAMhIiJTmECIiMiUou5EF5E2AGEAI0qpUKbriYjcIt1IrunJSQDp18uaAqasxlC0CURPHkqpYKFjISIqNrt+/OOU585cuAAA+PPnn095zfXp6atWY8hpE5aItIpIb5JjAS1BpLMBQLV2vS93URIRkRk5rYEopXpEpF3/WURateNBEWkTkYD2ujXh1iCAiPbfWgCtANiERUSkeeutt1Ke27JlS8ZrRMRyDPluwtoI4CXtdRiAD0BQKdWTeKGI7AWwC8AIgO68RUhERIbkO4FUJ/xcl+pCpVQETBxEREUr38N4I4g2SVmmNYH1iUjf5SLYmYuIyG3ynUDex0wtxAugN821aSmlupVSfqWUf0mK2ZhERJQ7OW3CEpEAAL+ItCqlerRO9T3a8WoO0SUisl+6znM75XoUVhBATcKxTu0lkwcRkYNxKRMiIjKFCYSIiExxdAIRke0i0j06OlroUIiIXMfRCUQpdUgp1VZVVVXoUIiIXMfRCYSIiDVATg0AAAS0SURBVAqHCYSIiExhAiEiIlOYQIiIyBRRShU6BstEZAzAx4WOo0jUAxgqdBBFgs9iBp/FDD6LqP+llKq0UkDR7kiYpY+VUv5CB1EMRKSPzyKKz2IGn8UMPosoEemzWgabsIiIyBQmECIiMqVUEgg3nprBZzGDz2IGn8UMPosoy8+hJDrRiYgo/0qlBkJERHnm6FFYItKK6Da5XqWUK6qlIlKN6G6OXgAblVId2vE5z8JNz0dE9rn9WYiID9G/F1BK9WjH3PosDP3epfwstN+tXSnVknDMtufi2BqI9gvqm1bpux+6wS4A/rgPiLZkz8JNz0f73bzaazc/i2e0vxe1IuJ167PQfqew9juGRcTnxmehf0bojD6DbJ6LYxMIgI0AwtrrMABfAWPJG20veP0bgRfR3z3Zs3DF8xER/RnoXPksRKQNwPsi4tX+jrj570UfgJf1GplSKgT3Pot4Rp+B4efi5ARSnfBzXUGiKBDtg3NE+5aQ7Fm45fl4tQ9LnVufRROiv9eIiHRpTZ2ufBZKqQiALgAvA9igHXbls0hg9BkYfi5OTiARALWFDqKAWpVS7drrZM+i5J+PiAT0anYcVz4LzXntw/MEgDa49FloTS5BpVQTgEhce77rnkUCo8/A8HNxcif6+5jJlF4AvQWMJa9EpFUp1am99iH5s6hOcqzUjGgfFtUAvC5/Fu9j5h99NaIfAmG481n49H8fAPYi2m/o1r8X8Yw+A8PPxbE1EK2DyKt/gCT5JlqStN93n4icEJETAGqTPQs3PB+lVEj7vWqh/YV38bPoAVCtd3hq/SCufBYAurXBJQEAu9z6LLTfyx/XKW7oGWTzXDiRkIiITHFsDYSIiAqLCYSIiExhAiEiIlOYQIiIyBQmEKICEJFWbdixnWUG7C6TKB0mEKLCqNWW2ICIVIvIHi2pBLQ/e5LdpJ27oi1doh/bIyJdiC7h4fqtWil/nDyRkKhUvIzoqqlhILZMzc5kFyqlgiKSuDpqKG5iaU4DJYrHGgiVPG011j16E4+I7NP+G1t5NP5aI+XZGJs+8S+2npf2uks7Xx0Xu17r6ALQHldM4tpFRHnBBEJuUYfo4pMhRJe60Gext8RfpDcrpWPkmix4EV12JNV7PIPouk5BaAsDxtdUtEUTw4n3E+UDEwiVPO3D2Bv3oRwBYt/+T+jXpet7yOaaLPVB28tEK9+r1ZBOaMnBh+j+Hj5otRKNXgsJ2JzQiAxjAiFXiVtwEYjWPoJanwMQ/TCv05qNupIWMHONN6FZyRTtwz+sx6DVLnoB9Gkr6/bGXxd3XzeA1rklEuUPEwiVPO3DWU8aXgD64nDDiDZnhYHYPhL6f88nK0u/BjOLzDVZjU8ptRNAqzYKqxXRWsd57VwnAJ++MF7CrcG434Uo7zgKi0qeliA6tdc9ccc7469LqJ0g4VybUqpbv0YpFdJe27IEeGIsRs7F7QdDVBCsgRDN8APQ+xOa4pq2gJlv+n4AevJoR0InPJGbcDl3IkQn4wEIx9dQEs77AATSXZPl+7VqZdnWAZ6LMonSYQIhIiJT2IRFRESmMIEQEZEpTCBERGQKEwgREZnCBEJERKYwgRARkSn/H0Ykj/MJbi8HAAAAAElFTkSuQmCC\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "if __name__ == \"\"\"__main__\"\"\":\n",
    "    \n",
    "    lhc_data = np.genfromtxt(\"lhc-data.dat\", dtype=float)\n",
    "    sm_central = np.genfromtxt(\"sm-prediction.dat\", dtype=float)\n",
    "    sm_lower = np.genfromtxt(\"sm-prediction-lower.dat\", dtype=float)\n",
    "    sm_upper = np.genfromtxt(\"sm-prediction-upper.dat\", dtype=float)\n",
    "    \n",
    "    # Tell pyplot to use LaTeX rendering\n",
    "    plt.rc('text', usetex=True)\n",
    "    plt.rc('font', family='serif')\n",
    "   \n",
    "    # Set x,y labels\n",
    "    plt.xlabel(\"$m_{j_1,j_2}\\quad \\mathrm{[GeV]}$\")\n",
    "    plt.ylabel(\"$\\mathrm{d}\\sigma /\\mathrm{d}m_{j_1,j_2}\\quad \\mathrm{[pb/GeV]}$\")\n",
    "    plt.yscale(\"log\")\n",
    "    plt.xlim(0,1000)\n",
    "    \n",
    "    ## Plot data\n",
    "    x_pts = np.array([point[0]+25 for point in lhc_data[:,0:1]])\n",
    "    y_pts = lhc_data[:,2]\n",
    "    \n",
    "    plt.errorbar(x_pts, y_pts, xerr=25., yerr=(lhc_data[:,3], lhc_data[:,4]),\n",
    "                 ls=\"\", color=\"black\", label=\"Experimental Data\")\n",
    "    \n",
    "    \n",
    "    \n",
    "    ## Plot central SM prediction\n",
    "    \n",
    "    # Step will skip the final bin, we need to extend the data set\n",
    "    x_bins = np.array([point for point in sm_central[:,0]])\n",
    "    y_values = np.array([point for point in sm_central[:,2]])\n",
    "    \n",
    "    # Artificially add another bin to complete the bins at 1000 GeV\n",
    "    x_bins_extended = np.append(x_bins, x_bins[-1]+x_bins[1])\n",
    "    # Give the same value as the final y value in the set for the\n",
    "    # final bin\n",
    "    y_values_extended = np.append(y_values, y_values[-1])\n",
    "    \n",
    "    plt.step(x_bins_extended, y_values_extended, where=\"post\", color=\"firebrick\",\n",
    "             alpha=0.75, linestyle=\"--\", label=\"Standard Model\")\n",
    "    \n",
    "    \n",
    "    ## Shade region between SM bounds\n",
    "    \n",
    "    # No need to produce new x_bins, same values as before\n",
    "    # Just extend y values for upper and lower bounds\n",
    "    y_lower = np.array([point for point in sm_lower[:,2]])\n",
    "    y_upper = np.array([point for point in sm_upper[:,2]])\n",
    "    \n",
    "    y_lower_extended = np.append(y_lower, y_lower[-1])\n",
    "    y_upper_extended = np.append(y_upper, y_upper[-1])\n",
    "\n",
    "    plt.fill_between(x_bins_extended, y_lower_extended, y_upper_extended, step=\"post\",\n",
    "                     color=\"firebrick\", alpha=0.25)\n",
    "    \n",
    "    plt.legend()\n",
    "\n",
    "    # Show plot\n",
    "    plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "___"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Analysis"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The data appears to show a **resonant feature** at $m_{jj}\\sim 850-900 \\,\\mathrm{GeV}$ !\n",
    "\n",
    "However, is this a statistically significant indication? The errors on the data are large."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "___\n",
    "**Exercise 2e**: Statistical analysis\n",
    "\n",
    "Using the composite Standard Model prediction (with upper and lower bounds), determine whether the experimental data displays more than **five-sigma** tension with the theoretical SM background.\n",
    "\n",
    "This means using the loaded histogram data for each, and comparing the **values** in the 850-900 $\\mathrm{GeV}$ bin to see whether statistical fluctuation is exceedingly unlikely as an explanation of the bump in the data!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Use everything you have worked with\n",
    "# above to perform the required statistical\n",
    "# analysis!\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Congratulations!\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
