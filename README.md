# New Physics Searches at the Large Hadron Collider

[Webpage live](https://hitham2496.gitlab.io/he-pheno-nuffield/)

### Nuffield Research Placement - Institute for Particle Physics Phenomenology, Durham University

## Introduction
The Standard Model (SM) of particle physics is the most well-tested theory in physics, with a wealth of data from experiments supporting the description of three of the four fundamental forces in the universe: the strong, electromagnetic, and weak fundamental forces.

However, we know that the SM is an incomplete model - notably excluding a description of [gravity](https://home.cern/science/physics/extra-dimensions-gravitons-and-tiny-black-holes), [neutrino masses](https://home.cern/science/accelerators/cern-neutrinos-gran-sasso), and [dark matter](https://home.cern/science/accelerators/cern-neutrinos-gran-sasso). Such models have to originate from physics beyond the Standard Model.

### **Higher precision**

Looking to the future of particle physics, the large hadron collider experiment is in the infancy of the [high-luminosity era](https://home.cern/science/accelerators/high-luminosity-lhc) for data collection - meaning high-precision experimental data can be collected and analysed and compared to precise theoretical predictions from the SM.

To enable the isolation of new physics signals, we require precise modelling of high-energy effects in the SM. Many sophisticated tools and frameworks have been developed to this end. This research project is centred around the framework of [_**High Energy Jets**_](https://hej.hepforge.org/) (HEJ) which produces high-energy theoretical predictions for particle collision experiments.

## Project Brief

The year is 2045. An experimental team from the LHC collaboration has discovered a potential signal for a new particle from beyond the standard model in analyses of Higgs boson production. This means theoretical predictions for Higgs production are required for both the strong-initiated process, and the weak-initiated process.

Higher precision on the side of the theoretical predictions is required for collisions at high-energies, which requires a systematic treatment. Specifically, the collision events in question show large separation in angle between the _jets_ of strongly-charged particles produced in the collision, i.e. the jets are produced with wide pair-wise angular separation. 

The approach of [HEJ](https://hej.hepforge.org/) is to apply precisely the required missing higher order corrections for wide-angle radiation, which the experimental teams hope will provide the most precise prediction for such events. When combined with precise descriptions of the weak-initiated process, this will allow for a conclusive isolation of the observed signal from theory and determine whether the data reflects new physics beyond the Standard Model.

Your task is to analyse the data in the context of the composite Standard Model predictions which aim to provide stable and precise descriptions of the Standard Model background. From this, you will be able to draw conclusions on how strong an indication of physics beyond the Standard Model the data presents. You will present your conclusions in two weeks' time in preparation for an upcoming conference on new physics at CERN.

Good luck!

## Resources

The code is hosted publicly on a [GitLab](https://gitlab.com/Hitham2496/he-pheno-nuffield/) repository.

To download and use the code you will need Python v3.6+, with the additional packages listed in 'requirements.txt' on the repository.

You can obtain a local copy by running the following in your terminal (in a folder you wish to save the files)

<pre>
  <code>
  git clone https://gitlab.com/Hitham2496/he-pheno-nuffield.git
  cd he-pheno-nuffield
  </code>
</pre>

Open <code>Jupyter</code> to interact with the python notebooks.

Alternatively, [Binder](https://mybinder.org/) may be used to interact with the notebooks **without** needing to install python or any packages locally.

There are two sets of exercises:

 <table>
  <tr>
    <th>Exercise</th>
    <th>Download Notebook</th>
    <th>Binder Notebook Link</th>
  </tr>
  <tr>
    <td>Introduction</td>
    <td>[Notebook](https://gitlab.com/Hitham2496/he-pheno-nuffield/-/tree/main/introduction_notebook)</td>
    <td>[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/Hitham2496%2Fhe-pheno-nuffield/HEAD)</td>
  </tr>
  <tr>
    <td>Data Analysis</td>
    <td>[Notebook](https://gitlab.com/Hitham2496/he-pheno-nuffield/-/tree/main/data_analysis)</td>
    <td>[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/Hitham2496%2Fhe-pheno-nuffield/HEAD)</td>
  </tr>
</table> 

Start with the introduction, and then move onto the data analysis.

## Authors, copyright, and acknowledgments
This project was created by [Hitham Hassan](mailto:hitham.t.hassan@durham.ac.uk?subject=[Nuffield Project] *your subject here*) and supervised jointly by: [Hitham Hassan](mailto:hitham.t.hassan@durham.ac.uk?subject=[Nuffield Project] *your subject here*), [Sebastian Jaskiewicz](mailto:sebastian.jaskiewicz@durham.ac.uk?subject=[Nuffield Project] *your subject here*), [Malina Rosca](mailto:malina.rosca@durham.ac.uk?subject=[Nuffield Project] *your subject here*), who are referred to cumulatively as *the authors* henceforth.

The work is licensed under a [Creative Commons Attribution 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/) license. The copyright for the material hosted on this page and in the associated GitLab repository rests with the authors, who permit others to use the resources for educational and non-profit purposes provided they [the authors] are accredited.

The authors give thanks to Yannich Ulrich for helping with this webpage, and the HEJ collaboration for the discussions, as well as the Nuffield Research Placement Program for providing students with invaluable research experience.
